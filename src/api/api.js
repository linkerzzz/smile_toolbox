import { wxRequest, wxRequestNoLoading } from '../utils/wxRequest'
// 登录
const loginApi = params => wxRequest(params, '/api/member/login')
const xiaoHsNoteDetail = params => wxRequest(params, '/api/wm')
// 解析小红书视频
const getXiaoHsVideo = params => wxRequest(params, '/api/xiaohongshu/geturl')
// 解析小红书图片
const getXiaoHsImage = params => wxRequest(params, '/api/xiaohongshu/geturl')
// 解析微视视频
const getWeishiVideo = params => wxRequest(params, '/api/weishi/geturl')
// 获取支付参数(预下单)
const getPrePay = params => wxRequest(params, '/api/pay/preorder')
// 获取支付结果
const getPayResult = params => wxRequest(params, '/api/order/pay_status')
// 头像小助手-获取图标列表
const getAvatarIcons = params => wxRequest(params, '/api/avatar/icons')
// 解析天猫种草猫视频和图片
const getZhongcmVideo = params => wxRequest(params, '/api/zhongcaomao/geturl')
// 短网址生成接口
const getShortUrl = params => wxRequest(params, '/api/dwz/create')
// 汉语字典
const getInfoByWord = params => wxRequest(params, '/api/tools/getInfoByWord')
// 增加积分
const addScore = params => wxRequestNoLoading(params, '/api/member/addScore')
// 减少积分
const decScore = params => wxRequestNoLoading(params, '/api/member/decScore')
// 查看积分
const getScore = params => wxRequestNoLoading(params, '/api/member/getScore')
// 举牌小人
const getHoldUpSign = params => wxRequest(params, '/api/tools/getHoldUpSignImage')
// 皮皮虾视频去水印
const getPipixiaVideo = params => wxRequest(params, '/api/pipixia/video/watermark')
// Likee短视频去水印
const getLikeeVideo = params => wxRequest(params, '/api/likee/video/watermark')
// 支持平台列表
const getSupportList = params => wxRequest(params, '/api/support-list')
// 获取配置项
const getConfig = params => wxRequest(params, '/api/config')
// 用户签到
const userSign = params => wxRequest(params, '/api/user/sign')
// 用户签到状态
const userSignCheck = params => wxRequest(params, '/api/user/check-sign')
// 去水印接口v2版本
const wmApiV2 = params => wxRequest(params, '/api/v2/wm')

module.exports = {
  loginApi,
  getXiaoHsVideo,
  getXiaoHsImage,
  getPrePay,
  getPayResult,
  getWeishiVideo,
  getAvatarIcons,
  getZhongcmVideo,
  xiaoHsNoteDetail,
  getShortUrl,
  getInfoByWord,
  addScore,
  decScore,
  getScore,
  getHoldUpSign,
  getPipixiaVideo,
  getLikeeVideo,
  getSupportList,
  getConfig,
  userSign,
  userSignCheck,
  wmApiV2
}
