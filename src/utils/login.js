import wepy from "wepy";
import { USER_INFO, SYSTEM_INFO } from "./constant";
//
// const loginUrl = apiBaseUrl + "/api/member/login";
// const updateUrl = apiBaseUrl + "/index/user/userupdate";
import tip from "./tip";
class Login {
    async login() {

        let apiBaseUrl = wepy.$appConfig.baseApiUrl;
        let loginUrl = apiBaseUrl + "/api/member/login";

        console.log("重新登录...")
        // return true;
        tip.loaded();
        let that = this;
        tip.loading('重新登陆...');
        let codeResult = await wepy.login();
        if (codeResult.code) {
            let sessionRes = await wepy.request({
                url: loginUrl,
                method: "POST",
                data: {
                    code: codeResult.code
                },
                header: { "Content-Type": "application/json" }
            });
            if (sessionRes.data.err_code == 0) {
                wepy.setStorageSync('session_id', sessionRes.data.session_id);
                wepy.setStorageSync('score', sessionRes.data.score);
            } else {
                tip.error("登陆失败");
            }

        }

        tip.loaded();
    }
}

export default new Login();
