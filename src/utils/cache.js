// 过期时间
var expireTime = '_expire'

/**
 * 设置缓存
 * @param {string} key 键名
 * @param {object|string,int} value 内容（支持字符串、json、数组、boolean等等）
 * @param {int} t 可选参数表示有效时间（单位：秒)
 */
function set(key, value, t) {
  wx.setStorageSync(key, value)
  var seconds = parseInt(t)
  if (seconds > 0) {
    var timestamp = Date.parse(new Date())
    timestamp = timestamp / 1000 + seconds
    wx.setStorageSync(key + expireTime, timestamp + '')
  } else {
    wx.removeStorageSync(key + expireTime)
  }
}

/**
 * 读取缓存
 * @param {*} key 键名
 * @param {*} def 可选参数,默认值
 */
function get(key, def) {
  var deadtime = parseInt(wx.getStorageSync(key + expireTime))
  if (deadtime) {
    if (parseInt(deadtime) < Date.parse(new Date()) / 1000) {
      if (def) { return def } else { return }
    }
  }
  var res = wx.getStorageSync(key)
  if (res) {
    return res
  } else {
    return def
  }
}

/**
 * 值是否存在
 * @param {*} key
 * @param {boolean} ignore 忽略时间
 */
function has(key, ignore) {
  if (ignore) {
    return !!wx.getStorageSync(key)
  }
  return typeof this.get(key) !== 'undefined'
}

/**
 * 移除指定key
 * @param {*} key
 */
function del(key) {
  wx.removeStorageSync(key)
  wx.removeStorageSync(key + expireTime)
}

function clear() {
  // wx.clearStorageSync()
}

module.exports = {
  set,
  get,
  has,
  del,
  clear
}
