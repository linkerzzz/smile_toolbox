// import AES from 'crypto-js/aes';
// import encode_utf8 from 'crypto-js/enc-utf8';

// 加密post请求参数
var encryptPost = function (data) {
    var key = 'aFSTJkmGbVnkQJBAP8g/tR/NDRS+cWO';
    return AES.encrypt(data,key).toString();
}
// 解密post接口返回的数据
var decryptPost = function (string) {
    var key = 'aFSTJkmGbVnkQJBAP8g/tR/NDRS+cWO';
    return AES.decrypt(string,key).toString(encode_utf8)
}

// 获取当前时间戳
function getCurrentTime() {
    var keep = "";
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? "0" + m : m;
    var d = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var h = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var f = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var s = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    var rand = Math.round(Math.random() * 899 + 100);
    keep = y + "" + m + "" + d + "" + h + "" + f + "" + s;
    return keep; //20160614134947
}
// object对象长度
function objLength(input) {
    var type = toString(input);
    var length = 0;
    if (type != "[object Object]") {
        //throw "输入必须为对象{}！"
    } else {
        for (var key in input) {
            if (key != "number") {
                length++;
            }
        }
    }
    return length;
}
//身份证号合法性验证
//支持15位和18位身份证号
//支持地址编码、出生日期、校验位验证
function vailIdcard(code) {
    var city={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江 ",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北 ",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏 ",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外 "};
    var tip = "";
    var pass= true;

    if(!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)){
        tip = "身份证号格式错误";
        pass = false;
    }

    else if(!city[code.substr(0,2)]){
        tip = "地址编码错误";
        pass = false;
    }
    else{
        //18位身份证需要验证最后一位校验位
        if(code.length == 18){
            code = code.split('');
            //∑(ai×Wi)(mod 11)
            //加权因子
            var factor = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ];
            //校验位
            var parity = [ 1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2 ];
            var sum = 0;
            var ai = 0;
            var wi = 0;
            for (var i = 0; i < 17; i++)
            {
                ai = code[i];
                wi = factor[i];
                sum += ai * wi;
            }
            var last = parity[sum % 11];
            if(parity[sum % 11] != code[17]){
                tip = "校验位错误";
                pass =false;
            }
        }
    }
    // if(!pass) alert(tip);
    return pass;
}
//验证是否是手机号码
function vailPhone(number) {
    let flag = false;
    let myreg = /^(((13[0-9]{1})|(14[0-9]{1})|(17[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/;
    if (number.length != 11) {
        flag = flag;
    } else if (!myreg.test(number)) {
        flag = flag;
    } else {
        flag = true;
    }
    return flag;
}
//验证是否西班牙手机(6开头 9位数)
function ifSpanish(number) {
    let flag = false;
    let myreg = /^([6|7|9]{1}(\d+){8})$/;
    if (number.length != 9) {
        flag = flag;
    } else if (!myreg.test(number)) {
        flag = flag;
    } else {
        flag = true;
    }
    return flag;
}
//浮点型除法
function div(a, b) {
    var c,
        d,
        e = 0,
        f = 0;
    try {
        e = a.toString().split(".")[1].length;
    } catch (g) {}
    try {
        f = b.toString().split(".")[1].length;
    } catch (g) {}
    return (
        (c = Number(a.toString().replace(".", ""))),
            (d = Number(b.toString().replace(".", ""))),
            mul(c / d, Math.pow(10, f - e))
    );
}
//浮点型加法函数
function accAdd(arg1, arg2) {
    var r1, r2, m;
    try {
        r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return ((arg1 * m + arg2 * m) / m).toFixed(2);
}
//浮点型乘法
function mul(a, b) {
    var c = 0,
        d = a.toString(),
        e = b.toString();
    try {
        c += d.split(".")[1].length;
    } catch (f) {}
    try {
        c += e.split(".")[1].length;
    } catch (f) {}
    return (
        Number(d.replace(".", "")) * Number(e.replace(".", "")) / Math.pow(10, c)
    );
}

// 遍历对象属性和值
function displayProp(obj) {
    var names = "";
    for (var name in obj) {
        names += name + obj[name];
    }
    return names;
}
// 去除字符串所有空格
function sTrim(text) {
    return text.replace(/\s/gi, "");
}
//去除所有冒号:
function replaceMaohao(txt) {
    return txt.replace(/\:/gi, "");
}
//转换星星分数
function convertStarArray(score) {
    //1 全星,0 空星,2半星
    var arr = [];
    for (var i = 1; i <= 5; i++) {
        if (score >= i) {
            arr.push(1);
        } else if (score > i - 1 && score < i + 1) {
            arr.push(2);
        } else {
            arr.push(0);
        }
    }
    return arr;
}
// 合并表单值
function mergeForm(form, data) {
    form = form.detail.value;
    for (var item in form) {
        data[item] = form[item];
    }
    return data;
}
// 判断是否为空
function isEmpty(params) {
    if (params == undefined || params == null) {
        return true;
    }
    params = params+'';
    params = sTrim(params);
    return params.length == 0;
}
// 日期月份/天的显示，如果是1位数，则在前面加上'0'
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate;
    return currentdate;
}
// 日期月份的显示，如果是1位数，则在前面加上'0'
function getNowMonth() {
    var date = new Date();
    var seperator1 = "-";
    var month = date.getMonth() + 1;
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    var currentdate = date.getFullYear() + seperator1 + month;
    return currentdate;
}

// 日期，在原有日期基础上，增加days天数，默认增加1天
function addDate(date, days) {
    if (days == undefined || days == '') {
        days = 1;
    }
    var date = new Date(date);
    date.setDate(date.getDate() + days);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return date.getFullYear() + '-' + getFormatDate(month) + '-' + getFormatDate(day);
}

//+---------------------------------------------------
//| 求两个时间的天数差 日期格式为 YYYY-MM-dd
//+---------------------------------------------------
function daysBetween(DateOne,DateTwo)
{
    var OneMonth = DateOne.substring(5,DateOne.lastIndexOf ('-'));
    var OneDay = DateOne.substring(DateOne.length,DateOne.lastIndexOf ('-')+1);
    var OneYear = DateOne.substring(0,DateOne.indexOf ('-'));

    var TwoMonth = DateTwo.substring(5,DateTwo.lastIndexOf ('-'));
    var TwoDay = DateTwo.substring(DateTwo.length,DateTwo.lastIndexOf ('-')+1);
    var TwoYear = DateTwo.substring(0,DateTwo.indexOf ('-'));

    var cha=((Date.parse(OneMonth+'/'+OneDay+'/'+OneYear)- Date.parse(TwoMonth+'/'+TwoDay+'/'+TwoYear))/86400000);
    return Math.abs(cha);
}

function getFormatDate(arg) {
    if (arg == undefined || arg == '') {
        return '';
    }
    var re = arg + '';
    if (re.length < 2) {
        re = '0' + re;
    }
    return re;
}

/**
 * 计算两个日期相差几天
 * cusDate 当前时间
 * oriDate  比较时间
 * 返回 正数为cusDate>oriDate
 */
function calculateTime(cusDate,oriDate) {
    var cusTime = cusDate.getTime();
    var oriTime = oriDate.getTime();
    return (cusTime-oriTime)/(1000*60*60*24)
}

/**
 * 字符串转时间（yyyy-MM-dd HH:mm:ss）
 * result （分钟）
 */
function stringToDate(fDate){
    var fullDate = fDate.split("-");
    return new Date(fullDate[0], fullDate[1]-1, fullDate[2], 0, 0, 0);
}


/**
 * 格式化日期
 * @param date 日期
 * @param format 格式化样式,例如yyyy-MM-dd HH:mm:ss E
 * @return 格式化后的金额
 */
function formatDate(date, format) {
    var v = "";
    if (typeof date == "string" || typeof date != "object") {
        return;
    }
    var year  = date.getFullYear();
    var month  = date.getMonth()+1;
    var day   = date.getDate();
    var hour  = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();
    var weekDay = date.getDay();
    var ms   = date.getMilliseconds();
    var weekDayString = "";

    if (weekDay == 1) {
        weekDayString = "星期一";
    } else if (weekDay == 2) {
        weekDayString = "星期二";
    } else if (weekDay == 3) {
        weekDayString = "星期三";
    } else if (weekDay == 4) {
        weekDayString = "星期四";
    } else if (weekDay == 5) {
        weekDayString = "星期五";
    } else if (weekDay == 6) {
        weekDayString = "星期六";
    } else if (weekDay == 7) {
        weekDayString = "星期日";
    }

    v = format;
    //Year
    v = v.replace(/yyyy/g, year);
    v = v.replace(/YYYY/g, year);
    v = v.replace(/yy/g, (year+"").substring(2,4));
    v = v.replace(/YY/g, (year+"").substring(2,4));

    //Month
    var monthStr = ("0"+month);
    v = v.replace(/MM/g, monthStr.substring(monthStr.length-2));

    //Day
    var dayStr = ("0"+day);
    v = v.replace(/dd/g, dayStr.substring(dayStr.length-2));

    //hour
    var hourStr = ("0"+hour);
    v = v.replace(/HH/g, hourStr.substring(hourStr.length-2));
    v = v.replace(/hh/g, hourStr.substring(hourStr.length-2));

    //minute
    var minuteStr = ("0"+minute);
    v = v.replace(/mm/g, minuteStr.substring(minuteStr.length-2));

    //Millisecond
    v = v.replace(/sss/g, ms);
    v = v.replace(/SSS/g, ms);

    //second
    var secondStr = ("0"+second);
    v = v.replace(/ss/g, secondStr.substring(secondStr.length-2));
    v = v.replace(/SS/g, secondStr.substring(secondStr.length-2));

    //weekDay
    v = v.replace(/E/g, weekDayString);
    return v;
}

/**
 * 接口加密
 * @param time 当时时间戳
 * @param params 参数
 */
function buildSign(time){
    // MD5(undefined&毫秒时间戳&appKey&data)
    var s = 'undefined' + '&' + time + '&' + '8JtwQB%D$ijPmOiY95eKnFIHl7@dX6BQU';
    return crypto.MD5(s);

}

/**
 * 加密请求参数
 * @param params
 */
function cryptoParams(time, params){
    var pwd = 'Jq0Kas08exEPKa#U%M0ZK7W5VHlFxAzrJ' + time;
    return sjcl.encrypt(pwd,params);
}

/**
 * 检查用户权限状态
 * @param scope
 */
function checkUserAuthScopes(scope){
        wx.getSetting({
            success: function(res) {
                console.log("用户当前设置:",res.authSetting)
                // var o = res.authSetting;
                // if (o[scope]) {
                //     getStorageSync(o[scope]);
                // }
                // o[scope] ? n() : 0 == o[e] ? wx.showModal({
                //     title: "提示",
                //     content: "需要打开微信相关设置",
                //     success: function(e) {
                //
                //     }
                // }) : n();
            },
            fail: function(e) {
                // t(e);
            }
        });
}

// 获取用户当前的积分
function getUserScore(){
    return wx.getStorageSync('score') * 1 || 0;
}

// 增加用户积分并返回增加后的积分
function incUserScore(inc){
    var score = this.getUserScore() + inc;
    return wx.setStorageSync('score',score) || score;
}
// 减少用户积分并返回增加后的积分
function decUserScore(dec){
    var score = this.getUserScore() - dec;
    return wx.setStorageSync('score',score) || score;
}

function getStorageSync(key){
    try {
        return wx.getStorageSync(key);
    } catch (e) {
        return null;
        return console.log(e), null;
    }
}

module.exports = {
    getCurrentTime: getCurrentTime,
    objLength: objLength,
    displayProp: displayProp,
    sTrim: sTrim,
    replaceMaohao: replaceMaohao,
    vailPhone: vailPhone,
    ifSpanish: ifSpanish,
    div: div,
    mul: mul,
    accAdd: accAdd,
    convertStarArray: convertStarArray,
    mergeForm: mergeForm,
    isEmpty:isEmpty,
    getNowFormatDate,
    getNowMonth,
    addDate,
    daysBetween,
    calculateTime,
    stringToDate,
    formatDate,vailIdcard,
    buildSign,
    cryptoParams,
    checkUserAuthScopes,
    getStorageSync,
    encryptPost,
    decryptPost,
    getUserScore,
    incUserScore,
    decUserScore
};
