import wepy from 'wepy'
import tip from './tip'
import login from './login'
import crypt from './secret/crypt'

const wxRequest = async (params = {}, url) => {
  let baseApiUrl = wepy.$appConfig.baseApiUrl
  tip.loading()
  let data = params || {}
  let header = {}
  data.weapp_id = 2
  let cerptData = crypt.encrypt(JSON.stringify(data))
  let nowTime = new Date().getTime()
  header['Content-Type'] = 'application/json'
  header['Cookie'] = 'weapi_session=' + wepy.getStorageSync('session_id')
  header['X-UUID'] = wepy.getStorageSync('openid')
  // 红薯原图
  header['APPID'] = 'wxa0e71d104ed4187c'

  let res = await wepy.request({
    url: baseApiUrl + url,
    method: params.method || 'POST',
    data: {
      'robot0105': cerptData,
      'timestamp': nowTime
    },
    header: header
  })
  if (res.data.err_code === 0 && res.data.data !== '' && typeof (res.data.data) !== 'object') {
    res.data.data = JSON.parse(crypt.decrypt(res.data.data))
    console.log(res.data.data)
  }
  tip.loaded()
  if (res.data.err_code === 400) { // session过期
    login.login()
  }
  return res.data
}

// 不显示等待浮层
const wxRequestNoLoading = async (params = {}, url) => {
  let baseApiUrl = wepy.$appConfig.baseApiUrl
  let data = params || {}
  let header = {}
  data.weapp_id = 2
  let cerptData = crypt.encrypt(JSON.stringify(data))
  let nowTime = new Date().getTime()
  header['Content-Type'] = 'application/json'
  header['Cookie'] = 'weapi_session=' + wepy.getStorageSync('session_id')
  header['X-UUID'] = wepy.getStorageSync('openid')
  let res = await wepy.request({
    url: baseApiUrl + url,
    method: params.method || 'POST',
    data: {
      'robot0105': cerptData,
      'timestamp': nowTime
    },
    header: header
  })
  if (res.data.err_code === 0 && res.data.data !== '' && typeof (res.data.data) !== 'object') {
    res.data.data = JSON.parse(crypt.decrypt(res.data.data))
  }
  if (res.data.err_code === 400) { // session过期
    login.login()
  }
  return res.data
}
// 参数不加密(调用第三方接口使用)
const wxRequestNoCrypt = async (params = {}, url) => {
  tip.loading()
  let data = params || {}
  let header = {}
  header['Content-Type'] = 'application/json'
  let res = await wepy.request({
    url: url,
    method: params.method || 'POST',
    data: data,
    header: header
  })
  return res.data
}

// 参数不加密(调用第三方接口使用)
const wxRequestThird = async (method = 'POST', params = {}, url, header = {}) => {
  tip.loading()
  let data = params || {}
  header['Content-Type'] = header.contentType || 'application/json'
  let res = await wepy.request({
    url: url,
    method: method,
    data: data,
    header: header
  }).catch(res => {
    console.log('请求超时')
    tip.loaded()
    tip.message('接口未知错误,请联系客服')
    return false
  })
  tip.loaded()
  return res.data
}

module.exports = {
  wxRequest,
  wxRequestNoLoading,
  wxRequestNoCrypt,
  wxRequestThird
}
