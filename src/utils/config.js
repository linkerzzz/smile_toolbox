// 工具列表
const toolList = [{
    id: 1,
    toolName: "redbook_del_watermark",
    text: "小红书去水印",
    icon: "/images/icon/icon_tool_red_book.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "enable",
    group: "pick",
    pack: "pages"
}, {
    id: 2,
    toolName: "add_watermark",
    text: "图片加水印",
    icon: "/images/icon/icon_tool_add_watermark.png",
    flag: "disable",
    group: "pick",
    pack: "pages"
}, {
    id: 3,
    toolName: "emoji_make",
    text: "表情包制作",
    icon: "/images/icon/icon_tool_emoji_make.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "disable",
    group: "pick",
    pack: "pages"
}, {
    id: 4,
    toolName: "bilibili_cover",
    text: "b站封面头像",
    icon: "/images/icon/icon_tool_bilibili_cover.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "enable",
    group: "pick",
    pack: 'packageA/pages'
}, {
    id: 5,
    toolName: "cut_image",
    text: "九宫格图片",
    icon: "/images/icon/icon_tool_cut_image.png",
    mark: "/images/icon/icon_mark_new.png",
    flag: "enable",
    group: "image",
    pack: "packageC/pages"
}, {
    id: 6,
    toolName: "append_image",
    text: "拼接图片",
    icon: "/images/icon/icon_tool_append_image.png",
    flag: "disable",
    group: "image"
}, {
    id: 7,
    toolName: "draw_board_new",
    text: "新版简易画板",
    icon: "/images/icon/icon_tool_draw_board.png",
    flag: "disable",
    group: "image"
}, {
    id: 8,
    toolName: "draw_board",
    text: "简易画板",
    icon: "/images/icon/icon_tool_draw_board.png",
    flag: "disable",
    group: "image"
}, {
    id: 9,
    toolName: "mosaic_image",
    text: "图片马赛克",
    icon: "/images/icon/icon_tool_mosaic_image.png",
    flag: "disable",
    group: "image"
}, {
    id: 10,
    toolName: "ascii_image",
    text: "字符画",
    icon: "/images/icon/icon_tool_ascii_image.png",
    flag: "disable",
    group: "image"
}, {
    id: 11,
    toolName: "scan_qrcode",
    text: "扫描二维码",
    icon: "/images/icon/icon_tool_scan_qrcode.png",
    flag: "enable",
    group: "qrcode",
    pack: "pages"
}, {
    id: 12,
    toolName: "generate_qrcode",
    text: "二维码生成",
    icon: "/images/icon/icon_tool_generate_qrcode.png",
    flag: "enable",
    group: "qrcode",
    pack: "pages"
}, {
    id: 13,
    toolName: "short_url",
    text: "短网址生成",
    icon: "/images/icon/icon_tool_short_url.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "enable",
    group: "other"
}, {
    id: 14,
    toolName: "other_lit_dict",
    text: "汉语字典",
    icon: "/images/icon/icon_tool_lit_dict.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "enable",
    pack: "packageA/pages",
    group: "other"
}, {
    id: 15,
    toolName: "life_time",
    text: "一生时间",
    icon: "/images/icon/icon_tool_life_time.png",
    flag: "disable",
    group: "other"
}, {
    id: 16,
    toolName: "joke_list",
    text: "每日笑话",
    icon: "/images/icon/icon_tool_joke_list.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "disable",
    group: "other"
}, {
    id: 17,
    toolName: "mobile_home",
    text: "号码归属地",
    icon: "/images/icon/icon_tool_mobile_home.png",
    mark: "/images/icon/icon_mark_wifi.png",
    flag: "disable",
    group: "other"
}, {
    id: 18,
    toolName: "simple_note",
    text: "便签条",
    icon: "/images/icon/icon_tool_simple_note.png",
    flag: "disable",
    group: "other"
}, {
    id: 19,
    toolName: "holiday_arrange",
    text: "放假安排",
    icon: "/images/icon/icon_tool_holiday_arrange.png",
    flag: "disable",
    group: "other"
}, {
    id: 20,
    toolName: "sum_calculator",
    text: "累加计算器",
    icon: "/images/icon/icon_tool_sum_calculator.png",
    flag: "disable",
    group: "other"
},{
    id: 21,
    toolName: "text_number_change",
    text: "数字转换",
    icon: "/images/icon/icon_text_number_change.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "text"
},{
    id: 22,
    toolName: "text_letter_change",
    text: "字母转换",
    icon: "/images/icon/icon_text_letter_change.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "text"
},{
    id: 23,
    toolName: "text_htz_change",
    text: "花体字转换",
    icon: "/images/icon/icon_text_htz_change.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "text"
},{
    id: 24,
    toolName: "text_nickname_change",
    text: "个性昵称",
    icon: "/images/icon/icon_text_nickname_change.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "text"
},{
    id: 25,
    toolName: "crypt_core_encode",
    text: "社会主义编码",
    icon: "/images/icon/icon_crypt_core_encode.png",
    flag: "enable",
    pack: "packageA/pages",
    group: "crypt"
}
,{
    id: 26,
    toolName: "other_calc",
    text: "超级计算器",
    icon: "/images/icon/icon_other_calc.png",
    flag: "disable",
    pack: "packCalc/pages",
    group: "other"
}
,{
    id: 27,
    toolName: "other_bmi_compute",
    text: "BMI健康指数",
    icon: "/images/icon/icon_other_bmi_compute.png",
    flag: "enable",
    pack: "packageA/pages",
    group: "other"
},{
    id: 28,
    toolName: "other_reaction_speed_test",
    text: "反应速度测试",
    icon: "/images/icon/icon_other_reaction_speed_test.png",
    flag: "disable",
    pack: "packageA/pages",
    group: "other"
},{
    id: 29,
    toolName: "other_batter_wallet",
    text: "微信充电余额",
    icon: "/images/icon/icon_other_batter_wallet.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "other"
},{
    id: 30,
    toolName: "other_system_info",
    text: "手机设备信息",
    icon: "/images/icon/icon_other_system_info.png",
    flag: "enable",
    pack: "packageB/pages",
    group: "other"
},{
        id: 31,
        toolName: "hold_up_sign",
        text: "小人举牌牌",
        icon: "/images/icon/icon_hold_up_sign.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "enable",
        pack: "packageB/pages",
        group: "pick"
    },{
        id: 32,
        toolName: "morse_code",
        text: "中文摩斯编码",
        icon: "/images/icon/icon_morse_code.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "enable",
        pack: "packageC/pages",
        group: "crypt"
    },{
        id: 33,
        toolName: "side_look_image",
        text: "斜看文字生成",
        icon: "/images/icon/icon_morse_code.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "disable",
        pack: "packageC/pages",
        group: "image"
    },{
        id: 34,
        toolName: "weishi_watermark",
        text: "微视去水印",
        icon: "/images/icon/icon_weishi_watermark.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "enable",
        pack: "packageC/pages",
        group: "image"
    },{
        id: 35,
        toolName: "pipixia_watermark",
        text: "皮皮虾去水印",
        icon: "/images/icon/icon_pipixia_watermark.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "enable",
        pack: "packageC/pages",
        group: "image"
    },{
        id: 36,
        toolName: "likee_watermark",
        text: "Likee去水印",
        icon: "/images/icon/icon_likee_watermark.png",
        mark: "/images/icon/icon_mark_new.png",
        flag: "enable",
        pack: "packageC/pages",
        group: "image"
    }];


// 帮助说明
const helpText = {
    '0':{
        'name':'首页-工具列表',
        'title':'关于',
        'desc':'『笑笑工具箱』是一款集各种在线小工具于一身的小程序，集成了【图像处理】\n（九宫格图片）\n【加密解密】\n（社会主义核心加密）\n【文字转换】\n（数字转换、字母转换、花体字转换、个性昵称）等功能，后续还有更多功能集成。\n---------\n很高兴您能选择笑笑工具箱，如果您觉得好用，也欢迎您点击屏幕右上角【•••】转发分享给更多需要的朋友使用，或者“发送到桌面”方便您下次使用。\n您的鼓励是我不懈前进的动力❤\n---------\n关于小程序的任何使用问题和bug，请点击工具浮窗的【吐槽】按钮反馈给我，我会第一时间回复。',
    },
    '1':{
        'name':'小红书去水印',
        'title':'小红书去水印',
        'desc':'',
    },
    '4':{
        'name':'b站封面头像',
        'title':'b站封面头像',
        'desc':'『基本说明』\n本工具提供了b站视频封面和up主的头像下载,暂不支持直播和番剧视频封面下载。\n『操作说明』\n1.输入对应的视频地址，或者视频对应的AV号，点击“获取图片”按钮\n2.下载完成后，则会在下方显示对应的封面，点击图片查看大图，长按进行保存',
    },
    '5':{
        'name':'九宫格图片',
        'title':'九宫格图片',
        'desc':'『基本说明』\n本工具提供了把一张图片切成九部分,支持批量保存。\n『操作说明』\n1.点击选择图片从相册选取想要分割的图片\n2.点击保存按钮',
    },
    '11':{
        'name':'扫描二维码',
        'title':'扫描二维码',
        'desc':'『基本说明』\n',
    },
    '32':{
        'name':'摩斯密码',
        'title':'摩斯密码',
        'desc':'『基本说明』\n',
    },
    '34':{
        'name':'微视去水印',
        'title':'微视去水印',
        'desc':'『基本说明』\n本工具提供了微视无水印视频的获取,支持保存至相册。\n『操作说明』\n1.打开微视短视频APP，点开某个视频，点击右下角分享按钮，在分享弹框中点击复制链接。\n2.打开本页面后链接会自动输入',
    }
}
// 接口相关配置
const apiConfig = {
    'secret':',@7Jidu#5|v%?{^a_oOL$lM~<;`BGPtY', // 加密秘钥
};

module.exports = {
    toolList,
    helpText,
    apiConfig,
};
