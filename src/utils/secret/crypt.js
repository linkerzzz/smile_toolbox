var CryptoJS = require('crypto-js');
var key = CryptoJS.enc.Utf8.parse("2ec9e65576c5c0b1"); //十六位十六进制数作为秘钥
var iv = CryptoJS.enc.Utf8.parse('7217ac017b4fb235'); //十六位十六进制数作为秘钥偏移量
//解密方法
function Decrypt(word) {
  var decrypt = CryptoJS.AES.decrypt(word, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });
  return decrypt.toString(CryptoJS.enc.Utf8);
}
//加密方法
function Encrypt(word) {
  var encrypted = CryptoJS.AES.encrypt(word, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  });
  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
  console.log('16进制字符串:',encrypted.toString())
  console.log("ppp:",encrypted)
  console.log('aaa:',CryptoJS.enc.Hex.parse(encrypted.ciphertext.toString()))
  console.log('base64:',CryptoJS.enc.Base64.stringify(encrypted.ciphertext))
  return encrypted.ciphertext.toString();
  return encrypted.ciphertext.toString().toUpperCase();
}
 
//暴露接口
module.exports = {
    encrypt:Encrypt,
    decrypt:Decrypt,
};