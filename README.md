
## 开发步骤

### 1. 安装 wepy
本项目基于wepy开发，[参考这里](https://github.com/wepyjs/wepy)
```bash
npm install wepy-cli -g
```

### 2. 下载源代码
```bash
git clone git@gitee.com:linkerzzz/smile_toolbox.git
```

### 3. 安装开发依赖
```bash
npm install
```

### 4. 编译源代码
```bash
// 开发模式
npm run dev
// 生产模式
npm run build
```
在开发者工具 勾选`详情-->本地配置-->启用自定义处理命令` 并设置上传前预处理命令`npm run build`



### 5.导入至开发者工具

编译完成后会生成`dist`目录，开发者工具本地开发目录指向`dist`目录。

**切记： 取消勾选`项目-->开启ES6转ES5`，否则代码运行报错。**


